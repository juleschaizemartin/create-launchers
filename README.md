# Create Launcher

Create Launcher is a .desktop launcher editor for Linux. It aims to be a
simple-to-use application, with no bells and whistles.

It is useful for programs available in AppImage format, or certain games that
don't create a launcher but the user has to open directly in a folder.

## Installation

Just run the *install.sh* file as your regular user (don't run it as root),
and it will install all the needed files in your personal folder. It will also
create a link in the Nautilus scripts folder. You don't need to open a terminal;
just open the folder in nautilus, right-click on it, and choose "Run as a program"
in the popup menu.

## Using it

Just open Nautilus, go where the executable for which you want to create a launcher
is, right-click on it, go to *scripts->create launcher*, and you will see the
interface.

Now, fill the name of the program (which will also be the name for the file),
the category, select a picture for the icon, and choose whether you want the
.desktop file created in the Desktop and/or in the Applications list (where you
can access it from the applications menu, the Gnome launchers...).

You can also use it to edit launchers that already exist. Just open the .desktop
file with Create Launcher.

## More info

The standard specification for desktop entries is available at

<https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html>

## License

This code is distributed under the GPLv3 license. Check the COPYING file for
details.

## Contacting the author

Sergio Costas  
rastersoft@gmail.com  
<https://www.rastersoft.com>
