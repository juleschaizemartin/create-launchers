#!/bin/bash

if [[ $EUID -eq 0 ]]; then
  echo "This script must NOT be run as root" 1>&2
  exit 1
fi

mkdir -p ~/.local/bin
mkdir -p ~/.local/share/create_launcher
cp create_launcher.js ~/.local/bin
cp create_launcher.glade ~/.local/share/create_launcher/
cp create_launcher.svg ~/.local/share/create_launcher/
chmod 755 ~/.local/bin/create_launcher.js
rm -f ~/.local/share/nautilus/scripts/"create launcher"
rm -f ~/.local/share/nautilus/scripts/"Create launcher"
ln ~/.local/bin/create_launcher.js ~/.local/share/nautilus/scripts/"Create launcher"

cp create_launcher.desktop ~/.local/share/applications/
echo TryExec=~/.local/bin/create_launcher.js >> ~/.local/share/applications/create_launcher.desktop
echo Exec=~/.local/bin/create_launcher.js >> ~/.local/share/applications/create_launcher.desktop
echo Icon=~/.local/share/create_launcher/create_launcher.svg >> ~/.local/share/applications/create_launcher.desktop
